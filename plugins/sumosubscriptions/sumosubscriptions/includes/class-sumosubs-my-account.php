<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Handle subscriptions in my account page.
 * 
 * @class SUMOSubs_My_Account
 */
class SUMOSubs_My_Account {

	/**
	 * Init SUMOSubs_My_Account.
	 */
	public static function init() {
		add_filter( 'woocommerce_account_menu_items', __CLASS__ . '::set_my_account_menu_items', 99 );

		add_action( 'woocommerce_account_sumo-subscriptions_endpoint', __CLASS__ . '::my_subscriptions' );
		add_shortcode( 'sumo_my_subscriptions', __CLASS__ . '::my_subscriptions', 10, 3 );

		add_action( 'woocommerce_account_view-subscription_endpoint', __CLASS__ . '::view_subscription' );
		add_action( 'sumosubscriptions_my_subscriptions_view-subscription_endpoint', __CLASS__ . '::view_subscription' );

		add_filter( 'user_has_cap', __CLASS__ . '::customer_has_capability', 10, 3 );
		add_filter( 'sumosubscriptions_my_subscription_table_pause_action', __CLASS__ . '::remove_pause_action', 10, 3 );
		add_filter( 'sumosubscriptions_my_subscription_table_cancel_action', __CLASS__ . '::remove_cancel_action', 10, 3 );
		add_action( 'wp_loaded', __CLASS__ . '::user_action', 20 );

		if ( isset( $_GET[ 'pay_for_order' ] ) ) {
			add_action( 'before_woocommerce_pay', __CLASS__ . '::wc_checkout_notice' );
		}
	}

	/**
	 * Set our menus under My account menu items
	 *
	 * @param array $items
	 * @return array
	 */
	public static function set_my_account_menu_items( $items ) {
		$endpoint = sumosubscriptions()->query->get_query_var( 'sumo-subscriptions' );
		$menu     = array( $endpoint => apply_filters( 'sumosubscriptions_my_subscriptions_table_title', __( 'My Subscriptions', 'sumosubscriptions' ) ) );
		$position = 2;
		$items    = array_slice( $items, 0, $position ) + $menu + array_slice( $items, $position, count( $items ) - 1 );
		return $items;
	}

	/**
	 * My Subscriptions template.
	 */
	public static function my_subscriptions( $atts = '', $content = '', $tag = '' ) {
		if ( is_null( WC()->cart ) || is_admin() ) {
			return;
		}

		global $wp;
		if ( 'sumo_my_subscriptions' === $tag ) {
			if ( ! empty( $wp->query_vars ) ) {
				foreach ( $wp->query_vars as $key => $value ) {
					// Ignore pagename param.
					if ( 'pagename' === $key ) {
						continue;
					}

					if ( has_action( 'sumosubscriptions_my_subscriptions_' . $key . '_endpoint' ) ) {
						do_action( 'sumosubscriptions_my_subscriptions_' . $key . '_endpoint', $value );
						return;
					}
				}
			}
		}

		$endpoint = sumosubscriptions()->query->get_query_var( 'sumo-subscriptions' );
		if ( isset( $wp->query_vars[ $endpoint ] ) && ! empty( $wp->query_vars[ $endpoint ] ) ) {
			$current_page = absint( $wp->query_vars[ $endpoint ] );
		} else {
			$current_page = 1;
		}

		$query = new WP_Query( apply_filters( 'woocommerce_my_account_my_sumo_subscriptions_query', array(
					'post_type'      => 'sumosubscriptions',
					'post_status'    => 'publish',
					'meta_key'       => 'sumo_get_user_id',
					'meta_value'     => get_current_user_id(),
					'fields'         => 'ids',
					'paged'          => $current_page,
					'posts_per_page' => 5,
				) ) );

		$customer_subscriptions = ( object ) array(
					'subscriptions' => $query->posts,
					'max_num_pages' => $query->max_num_pages,
					'total'         => $query->found_posts,
		);

		sumosubscriptions_get_template( 'subscriptions.php', array(
			'current_page'           => absint( $current_page ),
			'customer_subscriptions' => $customer_subscriptions,
			'subscriptions'          => $customer_subscriptions->subscriptions,
			'has_subscription'       => 0 < $customer_subscriptions->total,
			'endpoint'               => $endpoint,
		) );
	}

	/**
	 * My Subscriptions > View Subscription template.
	 *
	 * @param int $subscription_id
	 */
	public static function view_subscription( $subscription_id ) {
		if ( ! current_user_can( 'view-subscription', $subscription_id ) ) {
			echo '<div class="woocommerce-error">' . esc_html__( 'Invalid subscription.', 'sumosubscriptions' ) . ' <a href="' . esc_url( wc_get_page_permalink( 'myaccount' ) ) . '" class="wc-forward">' . esc_html__( 'My account', 'sumosubscriptions' ) . '</a></div>';
			return;
		}

		echo '<div class="sumo-view-subscription">';
		sumosubscriptions_get_template( 'view-subscription.php', array(
			'subscription_id'                                     => absint( $subscription_id ),
			'allow_subscribers_to_pause'                          => SUMOSubs_Admin_Options::get_option( 'allow_subscribers_to_pause' ),
			'allow_subscribers_to_pause_synced'                   => SUMOSubs_Admin_Options::get_option( 'allow_subscribers_to_pause_synced' ),
			'allow_subscribers_to_cancel'                         => SUMOSubs_Admin_Options::get_option( 'allow_subscribers_to_cancel' ),
			'allow_subscribers_to_select_resume_date'             => SUMOSubs_Admin_Options::get_option( 'allow_subscribers_to_select_resume_date' ),
			'allow_subscribers_to_turnoff_auto_renewals'          => SUMOSubs_Admin_Options::get_option( 'allow_subscribers_to_turnoff_auto_renewals' ),
			'allow_subscribers_to_switch_bw_identical_variations' => SUMOSubs_Admin_Options::get_option( 'allow_subscribers_to_switch_bw_identical_variations' ),
			'allow_subscribers_to_change_shipping_address'        => SUMOSubs_Admin_Options::get_option( 'allow_subscribers_to_change_shipping_address' ),
			'allow_subscribers_to_update_subscription_qty'        => SUMOSubs_Admin_Options::get_option( 'allow_subscribers_to_update_subscription_qty' ),
			'show_subscription_activities'                        => SUMOSubs_Admin_Options::get_option( 'show_subscription_activities' ),
			'max_pause_duration_for_subscribers'                  => absint( SUMOSubs_Admin_Options::get_option( 'max_pause_duration_for_subscribers' ) ),
		) );
		echo '</div>';
	}

	/**
	 * Checks if a user has a certain capability.
	 *
	 * @param array $allcaps All capabilities.
	 * @param array $caps    Capabilities.
	 * @param array $args    Arguments.
	 *
	 * @return array The filtered array of all capabilities.
	 */
	public static function customer_has_capability( $allcaps, $caps, $args ) {
		if ( isset( $caps[ 0 ] ) ) {
			switch ( $caps[ 0 ] ) {
				case 'view-subscription':
					$user_id         = absint( $args[ 1 ] );
					$subscription_id = absint( $args[ 2 ] );

					if ( sumo_is_subscription_exists( $subscription_id ) && absint( get_post_meta( $subscription_id, 'sumo_get_user_id', true ) ) === $user_id ) {
						$allcaps[ 'view-subscription' ] = true;
					}
					break;
			}
		}

		return $allcaps;
	}

	/**
	 * Hide Pause action from my Subscriptions table
	 *
	 * @param bool $action
	 * @param int $subscription_id
	 * @param int $parent_order_id
	 * @return bool
	 */
	public static function remove_pause_action( $action, $subscription_id, $parent_order_id ) {
		if ( 'Pending_Cancellation' === get_post_meta( $subscription_id, 'sumo_get_status', true ) ) {
			return false;
		}

		return $action;
	}

	/**
	 * Minimum waiting time for the User to get previlege to Cancel their Subscription.
	 * Show Cancel button only when the User has got the previlege
	 * 
	 * @param bool $action
	 * @param int $subscription_id
	 * @param int $parent_order_id
	 * @return bool
	 */
	public static function remove_cancel_action( $action, $subscription_id, $parent_order_id ) {
		$min_days_user_wait_to_cancel = absint( SUMOSubs_Admin_Options::get_option( 'allow_subscribers_to_cancel_after_schedule' ) );
		if ( 0 === $min_days_user_wait_to_cancel ) {
			return $action;
		}

		$order      = wc_get_order( $parent_order_id );
		$order_date = $order ? $order->get_date_created()->date_i18n( 'Y-m-d H:i:s' ) : '';

		if ( $min_days_user_wait_to_cancel > 0 && '' !== $order_date ) {
			$order_time                   = sumo_get_subscription_timestamp( $order_date );
			$min_time_user_wait_to_cancel = $order_time + ( $min_days_user_wait_to_cancel * 86400 );

			if ( sumo_get_subscription_timestamp() >= $min_time_user_wait_to_cancel ) {
				return $action;
			}
		}

		return false;
	}

	/**
	 * Perform user action.
	 */
	public static function user_action() {
		if ( ! isset( $_REQUEST[ 'wpnonce' ] ) ) {
			return;
		}

		if ( isset( $_REQUEST[ 'sumosubs-cancel-immediate' ] ) && wp_verify_nonce( sanitize_key( wp_unslash( $_REQUEST[ 'wpnonce' ] ) ), 'sumosubs-cancel-immediate-handler' ) ) {
			$subscription_id = absint( wp_unslash( $_REQUEST[ 'sumosubs-cancel-immediate' ] ) );

			//Cancel Subscription.
			if ( sumosubs_cancel_subscription( $subscription_id, array( 'request_by' => 'subscriber' ) ) ) {
				wc_add_notice( __( 'Subscription has been Cancelled successfully.', 'sumosubscriptions' ), 'success' );
			}

			wp_safe_redirect( sumo_get_subscription_endpoint_url( $subscription_id ) );
			exit;
		}
	}

	/**
	 * Prevent the User placing Paused/Cancelled Subscription renewal order from Pay for Order page.
	 */
	public static function wc_checkout_notice() {
		$renewal_order_id = sumosubs_get_subscription_renewal_order_in_pay_for_order();
		if ( ! $renewal_order_id ) {
			return;
		}

		$subscription_id = sumosubs_get_subscription_id_from_renewal_order( $renewal_order_id );
		switch ( get_post_meta( $subscription_id, 'sumo_get_status', true ) ) {
			case 'Pause':
				if ( 'yes' === SUMOSubs_Admin_Options::get_option( 'show_error_messages_in_pay_for_order_page' ) ) {
					wc_add_notice( SUMOSubs_Admin_Options::get_option( 'renewal_order_payment_in_paused_error_message' ), 'error' );
				}
				echo '<style>#order_review {display: none;}</style>';
				break;
			case 'Pending_Cancellation':
				if ( 'yes' === SUMOSubs_Admin_Options::get_option( 'show_error_messages_in_pay_for_order_page' ) ) {
					wc_add_notice( SUMOSubs_Admin_Options::get_option( 'renewal_order_payment_in_pending_cancel_error_message' ), 'error' );
				}
				echo '<style>#order_review {display: none;}</style>';
		}
	}

}

SUMOSubs_My_Account::init();
