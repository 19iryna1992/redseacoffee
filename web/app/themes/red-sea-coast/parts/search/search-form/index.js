(function ($) {

let searchForm = $('.JS--search-form');
let formHolder = $('.JS--search-form-holder');
let searchOpen = $('.JS--search-open');
let searchClose = $('.JS--close-search');

if (formHolder.length !== 0) {

    searchOpen.on('click', function () {
        formHolder.fadeIn(300);
    });

    searchClose.on('click', function () {
        $('.JS--search-input').val('');
        formHolder.fadeOut(300);
    });

    searchForm.on('submit', function (e) {
        let searchInputVal = $(this).find('.JS--search-input').val();
        if (searchInputVal.length) {
        } else {
            e.preventDefault();
        }
    });
}

})(jQuery);
