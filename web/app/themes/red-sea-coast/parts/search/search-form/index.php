<?php
load_blocks_script('search-form', 'red-sea-coast/search-form', [], 'parts/search');
echo load_inline_styles(__DIR__, 'search-form');
?>

<div class="search-form JS--search-form-holder">
    <div class="search-form__container">
        <form class="search-form__form JS--search-form" method="get" action="<?php echo home_url(); ?>" role="search">
            <input class="search-form__input JS--search-input" type="search" name="s" value="<?php the_search_query(); ?>" placeholder="<?php _e('Search', 'redSeaCoast'); ?>">
            <button class="search-form__submit" type="submit" role="button">
                <?php echo get_img('arrow_right') ?>
            </button>
            <div class="search-form__close JS--close-search">
                <?php echo get_img('close') ?>
            </div>
        </form>
    </div>
</div>
