<?php

echo load_inline_styles(__DIR__, 'search');

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$s = get_search_query();

if ($s) :

    woocommerce_product_loop_start();
    $args = array(
        's' => $s,
        'post_type' => 'product',
        'paged' => $paged,
    );

    $the_query = new WP_Query($args);

    if ($the_query->have_posts()) :

        while ($the_query->have_posts()) : $the_query->the_post();

                wc_get_template_part('content', 'product');
                
        endwhile;

    else : ?>

        <p class="search-alert">
            <?php _e('Whoops, we haven’t find any results.', 'redSeaCoast'); ?>
        </p>

    <?php endif;
    woocommerce_product_loop_end();
    wp_reset_postdata();
    ?>
<?php endif; ?>