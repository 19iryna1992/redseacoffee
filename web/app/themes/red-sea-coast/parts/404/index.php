<?php
$home_url = get_bloginfo('url');

load_inline_styles(__DIR__, 'parts/404');


?>
<h1 style="text-align:center;">
	<?php _e('404', 'redSeaCoast'); ?>
	<p><?php _e('Page not found', 'redSeaCoast'); ?></p>
</h1>

<a class='wp-block-button__link' href='<?php echo $home_url ?>'><?php _e('Home', 'redSeaCoast'); ?></a>
