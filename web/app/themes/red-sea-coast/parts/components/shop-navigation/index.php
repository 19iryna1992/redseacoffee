<?php
load_blocks_script('shop-navigation', 'red-sea-coast/shop-navigation', ['swiper'], 'parts/components');
load_inline_styles_plugin('swiper-bundle.min');
load_inline_styles_shared('sliders');
load_inline_styles(__DIR__, 'shop-navigation');

?>

<div class="woocommerce_navigation">
    <div class="swiper shopMenuSwiper">
        <?php wp_nav_menu(
            array(
                'theme_location' => 'shop_navigation',
                'container' => false,
                'menu_class' => 'swiper-wrapper',
                'walker' => new Main_Nav_Menu_Walker()
            )
        ); ?>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
</div>
