const shopMenuSection = document.querySelector('.shopMenuSwiper');

function runShopMenuSlider() {
    const swiper = new Swiper('.shopMenuSwiper', {
       slidesPerView: 1,
        loop: false,
        breakpoints: {
		
			480: {
				slidesPerView: 2,
				spaceBetween: 20
			},
			
            768: {
				slidesPerView: 3,
				spaceBetween:5
			},

			990: {
				slidesPerView: 3,
				spaceBetween: 30
			},

            1100: {
				slidesPerView: 5,
				spaceBetween: 30
			}

		},

        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
 

    shopMenuSection.addEventListener('keydown', (e) => {
        if (e.key === "ArrowLeft") {
            swiper.slidePrev();
        } else if (e.key === "ArrowRight") {
            swiper.slideNext();
        }
    });
}

document.addEventListener('DOMContentLoaded', () => {
    runShopMenuSlider();
});
