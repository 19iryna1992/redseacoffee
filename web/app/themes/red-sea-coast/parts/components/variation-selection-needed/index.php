<?php
load_inline_styles(__DIR__, 'variation-selection-needed');
load_blocks_script('variation-selection-needed', 'red-sea-coast/variation-selection-needed', [], 'parts/components');
?>

<div class="woocommerce-notices-wrapper woocommetce-notices-hide js--variation-selection-needed">
    <ul class="woocommerce-error" role="alert">
		<li>
		Please select some product options before adding this product to your cart.		
        </li>
	</ul>
</div>