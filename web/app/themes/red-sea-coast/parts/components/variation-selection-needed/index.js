const selectionNeeded = document.querySelector('.single_add_to_cart_button');
const selectionNeededNotes = document.querySelector('.js--variation-selection-needed');

selectionNeeded.addEventListener('click', () => {
    if (selectionNeeded.classList.contains('wc-variation-selection-needed')) {
        selectionNeededNotes.classList.remove('woocommetce-notices-hide');
    } else {
        selectionNeededNotes.classList.add('woocommetce-notices-hide');
    }
});
