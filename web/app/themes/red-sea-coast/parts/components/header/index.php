<?php
$main_logo = get_field('header_logo', 'option'); // ACF ID // icon name
$logo_home = get_field('header_logo_home', 'option');

load_blocks_script('header', 'red-sea-coast/header');
?>
<header class="main-header">
	<?php load_inline_styles(__DIR__, 'header'); ?>
	<div class="container main-header__container">
		<div class="main-header__logo">
			<a href="<?php echo get_bloginfo('url'); ?>" class="main-logo">
        <span class="screen-reader-text">
          <?php _e('Main Logo', 'redSeaCoast'); ?>
        </span>
        <div class="main-header__logo-global">
          <?php echo get_img($main_logo, 'full') ?>
        </div>
        <div class="main-header__logo-homepage">
          <?php echo get_img($logo_home, 'full') ?>
        </div>
      </a>
		</div>
		<?php if (has_nav_menu('navigation')) : ?>
			<div class="main-header__nav">
				<nav class="main-header__desktop-nav" aria-labelledby="nav-heading">
					<span id="nav-heading" class="screen-reader-text"><?php _e('Main Navigation', 'redSeaCoast'); ?></span>
					<?php wp_nav_menu(array('theme_location' => 'navigation', 'container' => false, 'walker' => new Main_Nav_Menu_Walker())); ?>
				</nav>
			</div>
		<?php endif; ?>
		<div class="main-header__wc">
			<ul class="main-header__wc-icons">
				<li class="main-header__wc-icon"><a class="JS--search-open" href="javascript:void(0);" aria-label="Search"><?php echo get_img('search', 'full') ?></a></li>
				<li class="main-header__wc-icon"><a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>" aria-label="Go to account page"><?php echo get_img('account', 'full') ?></a></li>
				<li class="main-header__wc-icon"><a href="<?php echo wc_get_cart_url() ?>" aria-label="Go to cart page"><?php echo get_img('cart', 'full') ?></a></li>
			</ul>
		</div>
		<button class="btn-menu" aria-haspopup="true" aria-expanded="false">
			<span></span>
			<span></span>
			<span></span>
			<span class="screen-reader-text"><?php _e('Menu', 'redSeaCoast'); ?></span>
		</button>
		<div class="main-header__mobile" hidden>
			<nav class="main-header__mobile-nav" aria-labelledby="mobile-nav-heading">
				<span id="mobile-nav-heading" class="screen-reader-text"><?php _e('Mobile Navigation', 'redSeaCoast'); ?></span>
				<?php wp_nav_menu(array('theme_location' => 'navigation', 'container' => false, 'walker' => new Main_Nav_Menu_Walker())); ?>
			</nav>
		</div>

		<?php echo get_part('search/search-form/index'); ?>
	</div>
</header>
