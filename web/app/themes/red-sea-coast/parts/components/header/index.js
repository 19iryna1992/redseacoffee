
const toggleLinks = document.querySelectorAll('.btn-menu, .menu-item__dropdown');
const mainHeader = document.querySelector('.main-header');
const body = document.body;
Array.from(toggleLinks).forEach(link => {
	link.addEventListener('click', function () {
		let expanded = this.getAttribute('aria-expanded') === 'true' || false;
		this.setAttribute('aria-expanded', !expanded);
		let menu = this.nextElementSibling;
		menu.hidden = !menu.hidden;
    if (this.classList.contains('btn-menu')) {
      if (!expanded && !mainHeader.classList.contains('expanded')) {
        mainHeader.classList.add('expanded');
        body.classList.add('scroll-lock');
      } else if (expanded && mainHeader.classList.contains('expanded')) {
        mainHeader.classList.remove('expanded');
        body.classList.remove('scroll-lock');
      }
    }
	});
});

const hoverSubmenu = document.querySelectorAll('.main-header__desktop-nav .menu-item-has-children');
Array.from(hoverSubmenu).forEach(link => {
	link.addEventListener('mouseover', function () {
		const dropdown = this.querySelector('.menu-item__dropdown');
		if (dropdown.getAttribute('aria-expanded') !== 'true') {
			let expanded = dropdown.getAttribute('aria-expanded') === 'true' || false;
			dropdown.setAttribute('aria-expanded', !expanded);
			const menu = this.querySelector('.menu-item__submenu')
			menu.hidden = false;
		}

	})
	link.addEventListener('mouseout', function () {
		const dropdown = this.querySelector('.menu-item__dropdown');
		let expanded = dropdown.getAttribute('aria-expanded') === 'true' || false;
		dropdown.setAttribute('aria-expanded', !expanded);
		const menu = this.querySelector('.menu-item__submenu');
		menu.hidden = true;
	})
});

const handleScroll = () => {
  let isScrolled = window.scrollY > 0 ? true : false;

  if (isScrolled) mainHeader.classList.add('scrolled');

  document.addEventListener('scroll', () => {
    if (!mainHeader.classList.contains('scrolled') && window.scrollY > 0) {
      mainHeader.classList.add('scrolled');
    } else if (window.scrollY === 0 && mainHeader.classList.contains('scrolled')) {
      mainHeader.classList.remove('scrolled');
    }
  });
}

handleScroll();
