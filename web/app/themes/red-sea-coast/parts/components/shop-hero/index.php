<?php

// ACF Fields
$hero_title = get_field('woocommerce_hero_title', 'option');

$image_size = 'full';
$image_attr = array("sizes" => "(min-width: 1270px) calc(50vw - ( 100vw - 1270px ) / 2), (min-width: 992px) 42.5vw, calc(100vw - (15px * 2))", "class" => "image");
$hero_bg = get_field('woocommerce_hero_background', 'option');
$image = wp_get_attachment_image($hero_bg, $image_size, "", $image_attr);

load_inline_styles(__DIR__, 'shop-hero');
?>

<section class="shop-hero">
    <?php if (!empty($image)) : ?>
        <div class="shop-hero__background">
            <?php echo $image; ?>
        </div>
    <?php endif; ?>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="shop-hero__page-title">
                    <h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
                </div>
            </div>
            <?php if (!empty($hero_title)) : ?>
                <div class="col-12">
                    <h2 class="shop-hero__title">
                        <?php echo $hero_title ?>
                    </h2>
                </div>
            <?php endif; ?>
            <div class="col-12">
                <?php echo get_part('components/shop-navigation/index'); ?>
            </div>
            <div class="col-12">
                <div class="shop-hero__line_decoration">
                    <?php echo get_img('line_decoration', 'full'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
