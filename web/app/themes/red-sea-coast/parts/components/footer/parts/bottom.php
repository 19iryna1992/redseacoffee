<?php
$footer_copyright = get_field('footer_copyright', 'option');
$footer_link_icon = get_field('footer_link_icon', 'option');
$footer_link = get_field('footer_link', 'option');
?>

<div class="container">
    <div class="main-footer__bottom">
        <div class="row">
            <?php if ($footer_copyright) : ?>
                <div class="col-12 col-lg-6">
                    <span class="main-footer__copyright">©<?php echo date('Y') . ' | ' . $footer_copyright ?></span>
                </div>
            <?php endif; ?>
            <?php if ($footer_link) : ?>
                <div class="col-12 col-lg-6">
                    <div class="main-footer__link">
                        <?php if ($footer_link_icon) : ?>
                            <span class="main-footet__link-icon"><?php echo get_img($footer_link_icon, 'full') ?></span>
                        <?php endif; ?>
                        <a href="<?php echo $footer_link['url'] ?>"><?php echo $footer_link['title'] ?></a>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>