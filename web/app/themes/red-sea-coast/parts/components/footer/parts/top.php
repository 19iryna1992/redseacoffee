<?php
$footer_logo = get_field('footer_logo', 'option');
$contact_title = get_field('footer_contact_title', 'option');
$address = get_field('footer_address', 'option');
$contact = get_field('footer_contact_info', 'option');
$site_map_title = get_field('footer_sitemap_title', 'option');
$logos_title = get_field('footer_logos_title', 'option');
?>

<div class="container">
    <div class="main-footer__top">
        <?php if ($footer_logo) : ?>
            <div class="row">
                <div class="col-12">
                    <div class="main-footer__logo">
                        <?php echo get_img($footer_logo, 'full') ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <?php if ($contact_title || $address || $contact) : ?>
                <div class="col-12 col-lg-5">
                  <div class="main-footer__address-container">
                    <?php if ($contact_title) : ?>
                        <h2 class="main-footer__title"><?php echo $contact_title ?></h2>
                    <?php endif; ?>
                    <div class="main-footer__content">
                        <?php if ($address) : ?>
                            <div class="main-footer__wyciwyg"><?php echo $address ?></div>
                        <?php endif; ?>
                        <?php if ($contact) : ?>
                            <div class="main-footer__wyciwyg"><?php echo $contact ?></div>
                        <?php endif; ?>
                    </div>
                  </div>
                </div>
            <?php endif; ?>
            <div class="col-12 col-lg-2 main-footer__column-nav">
                <div class="main-footer__nav-container">
                    <?php if ($site_map_title) : ?>
                        <h2 class="main-footer__title"><?php echo $site_map_title ?></h2>
                    <?php endif; ?>
                    <?php wp_nav_menu(
                        array(
                            'theme_location' => 'foot_navigation',
                            'container' => false,
                            'walker' => new Main_Nav_Menu_Walker()
                        )
                    ); ?>
                </div>
            </div>
            <div class="col-12 col-lg-5">
                <div class="main-footer__logos-container">
                    <?php if ($logos_title) : ?>
                        <h2 class="main-footer__title"><?php echo $logos_title ?></h2>
                    <?php endif; ?>
                    <?php if (have_rows('footer_logos', 'option')) : ?>
                        <div class="main-footer__logos">
                            <?php while (have_rows('footer_logos', 'option')) : the_row();
                                $logo = get_sub_field('logo');
                            ?>
                                <div class="main-footer__logos-item">
                                    <?php echo get_img($logo, 'full') ?>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>