<?php
load_inline_styles(__DIR__, 'shared-button');
load_blocks_script('shared-button', 'red-sea-coast/shared-button');

?>

<div class="shared_buttons">
    <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
        <a class="a2a_dd" href="https://www.addtoany.com/share">
            <?php echo get_img('shared', 'full') ?>
        </a>
    </div>
</div>

<script async src="https://static.addtoany.com/menu/page.js"></script>
