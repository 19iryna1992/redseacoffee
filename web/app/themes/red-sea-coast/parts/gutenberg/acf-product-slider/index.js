function runSlider() {
	const swiper = new Swiper('.product-slider__container', {
    slidesPerView: 1,
    spaceBetween: 10,
		loop: true,
		breakpoints: {
			// when window width is >= 480px
			480: {
				slidesPerView: 2,
				spaceBetween: 20
			},
			// when window width is >= 990px
			990: {
				slidesPerView: 3,
				spaceBetween: 30
			}
		},
		navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
		}
	});

  const productSlider = document.querySelector('.product-slider');

  if (productSlider !== null) {
    productSlider.addEventListener('keydown', (e) => {
      if (e.key === "ArrowLeft") {
        swiper.slidePrev();
      } else if (e.key === "ArrowRight") {
        swiper.slideNext();
      }
    });
  }
}

document.addEventListener('DOMContentLoaded', () => {
	runSlider();
});

if (window.acf) {
	window.acf.addAction('render_block_preview/type=slider', runSlider);
}
