<?php

/**
 * Block with Slider
 *
 * @package WordPress
 * @subpackage redSeaCoast
 * @since redSeaCoast 1.0
 */
$block_object = new Block($block);
$attr = $block_object->attr();
$name = $block_object->name();

// ACF

$slider_title = get_field('product_slider_title');

if (have_rows('product_slider_row')) :
?>
	<section <?php echo $attr; ?>>
		<?php load_inline_styles_plugin('swiper-bundle.min'); ?>
		<?php load_inline_styles_shared('sliders'); ?>
		<?php load_inline_styles(__DIR__, $name); ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-lg-7">
					<h2 class="<?php echo $name . '__title' ?>"><?php echo $slider_title ?></h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-12 col-lg-11">
					<div class="<?php echo $name . '__container' ?>   swiper-container" tabindex="0">
						<div class="slider__row swiper-wrapper">
							<?php
							$int = 1;
							while (have_rows('product_slider_row')) : the_row();
								$title = get_sub_field('title');
								$price = get_sub_field('price');
								$link = get_sub_field('link');
								$image_id = get_sub_field('image');
								$image = wp_get_attachment_image($image_id, 'full');

								if (!empty($image)) :
							?>
									<div class="product-slide swiper-slide">
										<?php if (!empty($link)) : ?>
											<a href="<?php echo $link; ?>" class="product-slide__card">
											<?php else : ?>
												<div class="product-slide__card">
												<?php endif; ?>
												<div class="product-slide__image"><?php echo $image; ?></div>
												<div class="product-slide__content">
													<?php if (!empty($title)) : ?>
														<h3 class="product-slide__title"><?php echo $title; ?></h3>
													<?php endif; ?>
													<?php if (!empty($price)) : ?>
														<h3 class="product-slide__price"><?php echo $price; ?></h3>
													<?php endif; ?>
												</div>
												<?php if (!empty($link)) : ?>
											</a>
										<?php else : ?>
									</div>
								<?php endif; ?>
						</div>
				<?php
									$int++;
								endif;
							endwhile;
				?>
					</div>
					<div class="swiper-button-next"></div>
					<div class="swiper-button-prev"></div>
					<!-- Add Pagination -->
					<div class=" product-slider__pagination swiper-pagination"></div>
				</div>
			</div>

		</div>
	</section>
<?php
endif;
