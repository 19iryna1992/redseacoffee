<?php

/**
 * Block Image Cards
 *
 * @package WordPress
 * @subpackage redSeaCoast
 * @since redSeaCoast 1.0
 */
$block_object = new Block($block);
$attr = $block_object->attr();
$name = $block_object->name();

// ACF

$title = get_field('wholesale_title');
$description = get_field('wholesale_description');
$contact_form_id = get_field('wholesale_form_id');
?>

<section <?php echo $attr; ?>>
	<?php echo load_inline_styles(__DIR__, $name); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-9">
				<?php if ($title) : ?>
					<h2 class="<?php echo $name . '__title' ?>">
						<?php echo $title ?>
					</h2>
				<?php endif; ?>
				<?php if ($description) : ?>
					<div class="<?php echo $name . '__description' ?>">
						<?php echo $description ?>
					</div>
				<?php endif; ?>
				<?php if ($contact_form_id) : ?>
					<div class="<?php echo $name . '__item' ?>">
						<?php echo do_shortcode('[contact-form-7 id="' . $contact_form_id . '"]'); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

