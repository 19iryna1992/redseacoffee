<?php

/**
 * Block Image Cards
 *
 * @package WordPress
 * @subpackage redSeaCoast
 * @since redSeaCoast 1.0
 */
$block_object = new Block($block);
$attr = $block_object->attr();
$name = $block_object->name();

?>
<?php if (have_rows('image_cards')) : ?>
	<section <?php echo $attr; ?>>
		<?php echo load_inline_styles(__DIR__, $name); ?>
		<div class="container">
			<div class="row justify-content-between">

				<?php while (have_rows('image_cards')) : the_row();
					$image = get_sub_field('image');
					$link = get_sub_field('link');
				?>
					<div class="col-12 col-md-4 text-center">
						<div class="<?php echo $name . '__card' ?>">
							<?php if (!empty($link)) : ?>
								<a href="<?php echo $link['url'] ?>" class="<?php echo $name . '__link' ?>" aria-label="<?php echo $link['title'] ?>">
							<?php endif; ?>

							<?php if (!empty($image)) : ?>
								<div class="<?php echo $name . '__img' ?>">
									<?php echo wp_get_attachment_image($image, array('550', '800')) ?>
								</div>
							<?php endif; ?>

							<?php if (!empty($link)) : ?>
								<p class="<?php echo $name . '__link-title' ?>"><?php echo $link['title'] ?></p>
							<?php endif; ?>

							<?php if (!empty($link)) : ?>
								</a>
							<?php endif; ?>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</section>
<?php endif; ?>