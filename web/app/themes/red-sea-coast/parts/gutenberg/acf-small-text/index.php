<?php

/**
 * Block Image Cards
 *
 * @package WordPress
 * @subpackage redSeaCoast
 * @since redSeaCoast 1.0
 */
$block_object = new Block($block);
$attr = $block_object->attr();
$name = $block_object->name();

// ACF

$background = get_field('small_text_background');
$title_color = get_field('title_color_part');
$title_white = get_field('title_white_part');
$content = get_field('content');
$link = get_field('link');

?>

<section <?php echo $attr; ?>>
	<?php echo load_inline_styles(__DIR__, $name); ?>
  <div class="<?php echo $name . '__background' ?>">
    <?php echo wp_get_attachment_image($background, 'full') ?>
  </div>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-9">
				<div class="small-text__wrap">
					<h2 class="<?php echo $name . '__title' ?>">
						<?php if ($title_color) : ?>
							<span class="<?php echo $name . '--title-color' ?>">
								<?php echo $title_color ?>
							</span>
						<?php endif; ?>
						<?php if ($title_white) : ?>
							<?php echo $title_white ?>
						<?php endif; ?>
					</h2>
					<?php if ($content) : ?>
						<div class="<?php echo $name . '__content' ?>">
							<?php echo $content ?>
						</div>
					<?php endif; ?>
					<?php if ($link) : ?>
						<?php echo get_button($link, 'outline', 'small-text__link'); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>