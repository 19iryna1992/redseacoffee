<?php

/**
 * Block Hero
 *
 * @package WordPress
 * @subpackage redSeaCoast
 * @since redSeaCoast 1.0
 */
$block_object = new Block($block);
$attr = $block_object->attr();
$name = $block_object->name();

?>

<section <?php echo $attr; ?>>
  <?php load_inline_styles_plugin('swiper-bundle.min'); ?>
  <?php load_inline_styles_shared('sliders'); ?>
  <?php echo load_inline_styles(__DIR__, $name); ?>
	<div class="container">
		<div class="row justify-content-center">
      <div class="col-12 col-lg-10">
        <div class="row <?php echo $name . '__items' ?>">
          <?php while(have_rows('product_cards_cards')) : the_row();
            $image = get_sub_field('image');
            $title = get_sub_field('title');
            $description = get_sub_field('description');
            $button = get_sub_field('link');
          ?>
            <div class="col-12 col-lg-3 product-card <?php echo $name . '__item' ?>">
              <?php if (!empty($image)) : ?>
                <span class="product-card__image">
                  <?php echo wp_get_attachment_image($image, 'full'); ?>
                </span>
              <?php endif; ?>
              <?php if (!empty($title)) : ?>
                <span class="product-card__title"><?php echo $title; ?></span>
              <?php endif; ?>
              <?php if (!empty($description)) : ?>
                <p class="product-card__description"><?php echo $description; ?></p>
              <?php endif; ?>
              <?php if (!empty($button)) : ?>
                <?php echo get_button($button, 'link', 'product-card__button'); ?>
              <?php endif; ?>
            </div>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
  </div>

</section>
