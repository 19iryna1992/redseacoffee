<?php

/**
 * Block Related Post
 *
 * @package WordPress
 * @subpackage redSeaCoast
 * @since redSeaCoast 1.0
 */
$block_object = new Block($block);
$attr = $block_object->attr();
$name = $block_object->name();

?>
<?php if (have_rows('related_post')) : ?>
	<section <?php echo $attr; ?>>
		<?php echo load_inline_styles(__DIR__, $name); ?>
		<div class="container">
			<div class="<?php echo $name . '__container' ?>">
				<?php while (have_rows('related_post')) : the_row();
					$image = get_sub_field('image');
					$title = get_sub_field('title');
					$link = get_sub_field('link');
				?>

					<div class="<?php echo $name . '__card' ?>" style="background-image: url('<?php echo $image ? $image : '' ?>');">
						<div class="<?php echo $name . '__overlay' ?>">
							<?php if (!empty($link)) : ?>
								<a href="<?php echo $link ?>" class="<?php echo $name . '__link' ?>">
							<?php endif; ?>
								<?php if (!empty($title)) : ?>
									<h3 class="<?php echo $name . '__title' ?>"><?php echo $title ?></h3>
								<?php endif; ?>
							<?php if (!empty($link)) : ?>
								</a>
							<?php endif; ?>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</section>
<?php endif; ?>
