function sliderController() {
	const swiper = new Swiper('.hero__container', {
    slidesPerView: 1,
    spaceBetween: 10,
		loop: true,
		breakpoints: {
			// when window width is >= 480px
			480: {
				slidesPerView: 2,
				spaceBetween: 20
			},
			// when window width is >= 990px
			990: {
				slidesPerView: 3,
				spaceBetween: 30
			}
		},
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
		}
	});
}

function wheelController() {
  const wheelTriggers = document.querySelectorAll('[data-wheel-trigger]');
  const showcaseItems = document.querySelectorAll('.hero__showcase-item');

  wheelTriggers.forEach(trigger => {
    trigger.addEventListener('focus', (e) => {
      triggerWheel(e.currentTarget);
    });

    let link = trigger.querySelector('a');
    if (link) {
      link.addEventListener('focus', (e) => {
        triggerWheel(e.currentTarget.parentNode.parentNode.parentNode);
      });
    }
  });

  showcaseItems.forEach(item => {
    item.addEventListener('focus', (e) => {
      const selectedItem = e.currentTarget;
      const currentActiveItem = document.querySelector('.hero__showcase-item--active');

      if (currentActiveItem && currentActiveItem !== selectedItem) {
        currentActiveItem.classList.remove('hero__showcase-item--active');
      }
      selectedItem.classList.add('hero__showcase-item--active');
    });

    let link = item.querySelector('a');
    if (link) {
      link.addEventListener('focus', (e) => {
        const selectedItem = e.currentTarget.parentNode.parentNode.parentNode;
        const currentActiveItem = document.querySelector('.hero__showcase-item--active');

        if (currentActiveItem && currentActiveItem !== selectedItem) {
          currentActiveItem.classList.remove('hero__showcase-item--active');
        }
        selectedItem.classList.add('hero__showcase-item--active');
      })
    }
  })
}

function triggerWheel(trigger) {
  const wheel = document.querySelector('.hero__wheel');
  const wheelOptions = [
    'top-left',
    'top-right',
    'bottom-right',
    'bottom-left'
  ];
  const oldQuarter = document.querySelector('.hero__showcase-item--active').dataset.wheelTrigger;
  const newQuarter = trigger.dataset.wheelTrigger;
  const selectedImage = document.querySelector(`[data-wheel-image="${newQuarter}"]`);
  const currentWheelRotation = parseInt(wheel.dataset.rotation);

  let newRotation = currentWheelRotation + 90 * (wheelOptions.indexOf(newQuarter) - wheelOptions.indexOf(oldQuarter));

  if (wheelOptions.indexOf(oldQuarter) - wheelOptions.indexOf(newQuarter) === 3) {
    newRotation = currentWheelRotation + 90;
  } else if (wheelOptions.indexOf(oldQuarter) - wheelOptions.indexOf(newQuarter) === -3) {
    newRotation = currentWheelRotation - 90;
  }

  wheel.style.transform = `translate(-50%, -50%) rotate(${newRotation}deg)`;
  wheel.dataset.rotation = newRotation;

  let currentActiveImage = document.querySelector('.hero__products-item--active');

  if (currentActiveImage && currentActiveImage !== selectedImage) {
    currentActiveImage.classList.remove('hero__products-item--active');
    selectedImage.classList.add('hero__products-item--active');
  }
}

document.addEventListener( 'DOMContentLoaded', () => {
  sliderController();
	wheelController();
});

if ( window.acf ) {
	window.acf.addAction( 'render_block_preview/type=hero', sliderController);
	window.acf.addAction( 'render_block_preview/type=hero', wheelController);
}
