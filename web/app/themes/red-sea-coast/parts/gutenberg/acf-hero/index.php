<?php

/**
 * Block Hero
 *
 * @package WordPress
 * @subpackage redSeaCoast
 * @since redSeaCoast 1.0
 */
$block_object = new Block($block);
$attr = $block_object->attr();
$name = $block_object->name();

// ACF
$background = get_field('hero_video_poster');
$video_background_file = get_field('hero_video_background_file');
$video_background_file_mob = get_field('hero_video_background_file_mobile');
$header_image = get_field('hero_header_image');
$button = get_field('hero_button');
$wheel_top_left = get_field('hero_top_left');
$wheel_top_right = get_field('hero_top_right');
$wheel_bottom_right = get_field('hero_bottom_right');
$wheel_bottom_left = get_field('hero_bottom_left');
$footer = get_field('hero_footer');

$wheel_top_left['slug'] = 'top-left';
$wheel_top_right['slug'] = 'top-right';
$wheel_bottom_right['slug'] = 'bottom-right';
$wheel_bottom_left['slug'] = 'bottom-left';

$wheel_parts = [
  $wheel_top_left,
  $wheel_top_right,
  $wheel_bottom_right,
  $wheel_bottom_left
];
?>

<section <?php echo $attr; ?>>
  <?php load_inline_styles_plugin('swiper-bundle.min'); ?>
  <?php load_inline_styles_shared('sliders'); ?>
  <?php echo load_inline_styles(__DIR__, $name); ?>
  <?php if (!empty($video_background_file)) : ?>
    <div class="<?php echo $name . '__background ' . $name . '__background--main ' . $name . '__background--media' ?>">
      <video autoplay muted loop playsinline preload="none" width="100%">
        <source src="<?php echo $video_background_file['url'] ?>" type="<?php echo $video_background_file['mime_type'] ?>" />
      </video>
    </div>
  <?php endif; ?>
  <?php if (!empty($video_background_file_mob)) : ?>
    <div class="<?php echo $name . '__background ' . $name . '__background--mob ' . $name . '__background--media' ?>">
      <video autoplay muted loop playsinline preload="none" width="100%">
        <source src="<?php echo $video_background_file_mob['url'] ?>" type="<?php echo $video_background_file_mob['mime_type'] ?>" />
      </video>
    </div>
  <?php endif; ?>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-10 col-lg-4">
        <?php echo wp_get_attachment_image($header_image, 'full') ?>
        <?php if ($button) : ?>
          <?php echo get_button($button, 'fill', "{$name}__button"); ?>
        <?php endif; ?>
      </div>
    </div>

    <div class="<?php echo $name . '__showcase ' . $name . '__showcase--mobile' ?>">
      <div class="<?php echo $name . '__container' ?> swiper-container">
        <div class="slider__row swiper-wrapper">
          <?php foreach ($wheel_parts as $part) :
            if ($part['image']) : ?>
              <div class="<?php echo $name . '-slide'; ?> swiper-slide">
                <div class="<?php echo $name . '-slide__card'; ?>">
                  <div class="<?php echo $name . '-slide__image'; ?>">
                    <?php echo wp_get_attachment_image($part['image'], 'full') ?>
                  </div>
                  <div class="<?php echo $name . '-slide__content'; ?>">
                    <span class="<?php echo $name . '__showcase-title' ?>"><?php echo $part['name']; ?></span>
                    <div class="<?php echo $name . '__showcase-content' ?>">
                      <p class="<?php echo $name . '__showcase-description' ?>"><?php echo $part['description']; ?></p>
                      <?php if ($part['link']) : ?>
                        <?php echo get_button($part['link'], 'arrow', "{$name}__showcase-read-more"); ?>
                      <?php endif; ?>
                    </div>
                  </div>
                </div>
              </div>
          <?php endif;
          endforeach; ?>
        </div>
        <!-- Add Pagination -->
        <div class=" <?php echo $name . '-slider__pagination'; ?> swiper-pagination"></div>
      </div>
    </div>
  </div>

  <div class="<?php echo $name . '__showcase ' . $name . '__showcase--desktop' ?>">
    <div class="<?php echo $name . '__showcase-items' ?>">
      <?php foreach ($wheel_parts as $index => $part) : ?>
        <article tabindex="0" class="<?php echo $name . '__showcase-item ' . ($index === 0 ? $name . '__showcase-item--active ' : '') . $name . '__showcase-item--' . $part['slug'] ?>" data-wheel-trigger="<?php echo $part['slug'] ?>">
          <span class="<?php echo $name . '__showcase-title' ?>"><?php echo $part['name']; ?></span>
          <div class="<?php echo $name . '__showcase-content' ?>">
            <p class="<?php echo $name . '__showcase-description' ?>"><?php echo $part['description']; ?></p>
            <?php if ($part['link']) : ?>
              <?php echo get_button($part['link'], 'arrow', "{$name}__showcase-read-more"); ?>
            <?php endif; ?>
          </div>
        </article>
      <?php endforeach; ?>
    </div>

    <div class="<?php echo $name . '__products' ?>">
      <?php foreach ($wheel_parts as $index => $part) : ?>
        <article class="<?php echo $name . '__products-item ' . ($index === 0 ? $name . '__products-item--active ' : '') . $name . '__products-item--' . $part['slug'] ?>" data-wheel-image="<?php echo $part['slug'] ?>">
          <?php echo wp_get_attachment_image($part['image'], 'full') ?>
        </article>
      <?php endforeach; ?>
    </div>

    <div class="<?php echo $name . '__wheel' ?>" data-rotation="0"></div>
  </div>

  <footer class="<?php echo $name . '__footer' ?>">
    <p class="<?php echo $name . '__footer-title' ?>"><?php echo $footer['top_text']; ?></p>
    <div class="<?php echo $name . '__footer-bottom' ?>">
      <p class="<?php echo $name . '__footer-bottom-left' ?>"><?php echo $footer['bottom_left_text']; ?></p>
      <span class="<?php echo $name . '__footer-bottom-separator' ?>">
        <?php echo wp_get_attachment_image($footer['bottom_separator'], 'full') ?>
      </span>
      <p class="<?php echo $name . '__footer-bottom-right' ?>"><?php echo $footer['bottom_right_text']; ?></p>
    </div>
  </footer>

</section>