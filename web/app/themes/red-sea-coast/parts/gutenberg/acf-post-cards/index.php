<?php

/**
 * Block Post Cards
 *
 * @package WordPress
 * @subpackage redSeaCoast
 * @since redSeaCoast 1.0
 */
$block_object = new Block($block);
$attr = $block_object->attr();
$name = $block_object->name();

?>
<?php if (have_rows('post_cards')) : ?>
	<section <?php echo $attr; ?>>
		<?php echo load_inline_styles(__DIR__, $name); ?>
		<div class="<?php echo $name . '__container' ?>">
			<?php while (have_rows('post_cards')) : the_row();
				$image = get_sub_field('image');
        $image_overlay = get_sub_field('image_overlay');
				$link = get_sub_field('link');
				$category = get_sub_field('category');
				$date = get_sub_field('date');
				$description = get_sub_field('description');
			?>
				<div class="<?php echo $name . '__card' ?>" style="background-image: url('<?php echo $image ? $image : '' ?>');">
         <div class="<?php echo $name . '__card-background' ?>" style="background: linear-gradient(180deg, rgba(132, 85, 17, 0) 27.61%, <?php echo $image_overlay ?> 100%);"></div>
					<?php if ($link) : ?>
						<a href="<?php echo $link['url'] ?>" class="<?php echo $name . '__link' ?>" aria-label="<?php echo $link['title'] ?>">
					<?php endif; ?>
					<div class="<?php echo $name . '__card-info' ?>">
						<?php if ($category || $date) : ?>
							<div class="<?php echo $name . '__info' ?>">
								<?php if ($category) : ?>
									<span class="<?php echo $name . '__category' ?>"><?php echo $category ?></span>
								<?php endif; ?>
								<?php if ($category && $date) :
									echo ' / ';
								endif; ?>
								<?php if ($date) : ?>
									<span class="<?php echo $name . '__date' ?>"><?php echo $date ?></span>
								<?php endif; ?>
							</div>
						<?php endif; ?>
						<?php if ($link) : ?>
							<h2 class="<?php echo $name . '__title' ?>"><?php echo $link['title'] ?></h2>
						<?php endif; ?>
						<?php if ($description) : ?>
							<p class="<?php echo $name . '__description' ?>"><?php echo $description ?></p>
						<?php endif; ?>
					</div>
          <?php if ($link) : ?>
						</a>
					<?php endif; ?>
				</div>
			<?php endwhile; ?>
		</div>
	</section>
<?php endif; ?>