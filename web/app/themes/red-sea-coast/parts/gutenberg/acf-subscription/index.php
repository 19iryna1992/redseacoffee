<?php

/**
 * Block  Subscription
 *
 * @package WordPress
 * @subpackage redSeaCoast
 * @since redSeaCoast 1.0
 */
$block_object = new Block($block);
$attr = $block_object->attr();
$name = $block_object->name();


// ACF
$image_id = get_field('subscription_image');
$image_size = array('700', '700');
$image_attr = array("sizes" => "(min-width: 1270px) calc(50vw - ( 100vw - 1270px ) / 2), (min-width: 992px) 42.5vw, calc(100vw - (15px * 2))", "class" => "image");
$image = wp_get_attachment_image( $image_id, $image_size, "", $image_attr );

$title = get_field('subscription_title');
$description = get_field('subscription_description');
$link = get_field('subscription_link');

?>
<section <?php echo $attr; ?>>

	<?php echo load_inline_styles(__DIR__, $name); ?>

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="<?php echo $name . '__container' ?>">
					<?php if ($image) : ?>
						<div class="<?php echo $name . '__img' ?>">
						<?php echo $image; ?>
						</div>
					<?php endif; ?>
					<div class="<?php echo $name . '__content' ?>">
						<div class="<?php echo $name . '__content-wrapper' ?>">
							<?php if ($title) : ?>
								<h2 class="<?php echo $name . '__title' ?>">
									<?php echo $title ?>
								</h2>
							<?php endif; ?>
							<?php if ($description) : ?>
								<div class="<?php echo $name . '__description' ?>">
									<?php echo $description ?>
								</div>
							<?php endif; ?>
							<?php if ($link) : ?>
								<?php echo get_button($link, 'wp-block-button', ''); ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
