<?php

/**
 * Block Image Cards
 *
 * @package WordPress
 * @subpackage redSeaCoast
 * @since redSeaCoast 1.0
 */
$block_object = new Block($block);
$attr = $block_object->attr();
$name = $block_object->name();

// ACF
$type = get_field('line_decoration_type') == 'short' ? '--img-short' : '--img-full';

?>


<section <?php echo $attr; ?>>
	<?php echo load_inline_styles(__DIR__, $name); ?>
	<div class="container">
		<div class="<?php echo $name . '__wrap' ?>">
			<div class="<?php echo $name . $type ?>">
				<?php if ($type == '--img-short') :
					echo get_img('small_line_dekoration', 'full');
				else :
					echo get_img('line_decoration', 'full');
				endif; ?>
			</div>
		</div>
	</div>
</section>