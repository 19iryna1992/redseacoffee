function runSlider() {
	const swiper = new Swiper('.slider__container', {
		loop: true,
		autoHeight: true,
		pagination: {
			el: '.swiper-pagination',
			type: 'fraction',
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
	});
}

document.addEventListener( 'DOMContentLoaded', () => {
	runSlider();
});

if ( window.acf ) {
	window.acf.addAction( 'render_block_preview/type=slider', runSlider);
}
