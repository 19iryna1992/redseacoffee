<?php
$name = 'slider';
$title = str_replace( '_', ' ', ucfirst($name) );

acf_register_block_type( array(
	'name'              => $name,
	'title'             => __($title, 'redSeaCoast'),
	'description'       => __("$title block", 'redSeaCoast'),
	'category'          => 'theme_blocks',
	'icon'              => 'welcome-view-site',
	'mode'              => 'edit',
	'keywords'          => array( $title ),
	'supports'          => array(
		'align' => false,
		'anchor' => true,
	),
	'render_template'   => get_template_directory() . "/parts/gutenberg/acf-$name/index.php",
	'enqueue_assets'  => function() use ( $name ) {
		wp_enqueue_script(
			"redSeaCoast/$name",
			get_template_directory_uri() . "/parts/gutenberg/acf-$name/index.min.js",
			['swiper'],
			filemtime( get_template_directory() . "/parts/gutenberg/acf-$name/index.min.js" ),
			true
		);
	}
));
