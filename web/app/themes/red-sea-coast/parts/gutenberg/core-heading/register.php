<?php
function insert_heading_styles( $block_content, $block ) {
	if ( 'core/heading' === $block['blockName'] ) {
		$block_content = '<div data-key="core-heading"></div>' . $block_content;
	}
	return $block_content;
}
add_filter( 'render_block', 'insert_heading_styles', 10, 2 );
