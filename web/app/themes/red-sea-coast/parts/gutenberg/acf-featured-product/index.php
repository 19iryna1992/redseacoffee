<?php

/**
 * Block Featured Image
 *
 * @package WordPress
 * @subpackage redSeaCoast
 * @since redSeaCoast 1.0
 */
$block_object = new Block($block);
$attr = $block_object->attr();
$name = $block_object->name();

// ACF

$section_title = get_field('featured-product_section_title');
$product_img = get_field('featured-product_image');
$product_name = get_field('featured-product_product_name');
$product_description = get_field('featured-product_description');
$region_img = get_field('featured-product_region_image');
$region_name = get_field('featured-product_region_name');
$product_info_title_color = get_field('featured-product_info_color_title');
$product_info_title = get_field('featured-product_info_title');
$product_info_description = get_field('featured-product_info_description');


?>

<section <?php echo $attr; ?>>
	<?php echo load_inline_styles(__DIR__, $name); ?>
	<div class="container">
		<div class="<?php echo $name . '__arabic-calligraphy' ?>">
			<?php echo get_img('arabic-calligraphy', 'full') ?>
		</div>
		<div class="row justify-content-center">
			<div class="col-12 col-lg-9">
				<div class="<?php echo $name . '__wrap' ?>">
					<div class="<?php echo $name . '__item' ?>">
						<?php if ($section_title) : ?>
							<h3 class="<?php echo $name . '__title' ?>">
								<?php echo $section_title ?>
							</h3>
						<?php endif; ?>
						<?php if ($product_img) : ?>
							<div class="<?php echo $name . '__item-img' ?>">
								<?php echo wp_get_attachment_image($product_img, 'full'); ?>
							</div>
						<?php endif; ?>
						<?php if ($product_name) : ?>
							<div class="<?php echo $name . '__item-name' ?>">
								<?php echo $product_name ?>
							</div>
						<?php endif; ?>
						<?php if ($product_description) : ?>
							<div class="<?php echo $name . '__item-description' ?>">
								<?php echo $product_description ?>
							</div>
						<?php endif; ?>
					</div>

					<div class="<?php echo $name . '__info' ?>">
						<?php if ($region_img) : ?>
							<div class="<?php echo $name . '__region' ?>">
								<?php echo get_img($region_img, array('385', '245')) ?>
							</div>
						<?php endif; ?>
						<div class="<?php echo $name . '__content-wrapper' ?>">
							<div class="<?php echo $name . '__content' ?>">

								<?php if ($product_info_title || $product_info_title_color) : ?>
									<h2 class="<?php echo $name . '__info-title' ?>">
										<?php if ($product_info_title) : ?>
											<span class="<?php echo $name . '__info-title--color' ?>">
												<?php echo $product_info_title_color ?>
											</span>
										<?php endif; ?>
										<?php echo $product_info_title ?>
									</h2>
								<?php endif; ?>
								<?php if ($product_info_description) : ?>
									<div class="<?php echo $name . '__info-description' ?>">
										<?php echo $product_info_description ?>
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>