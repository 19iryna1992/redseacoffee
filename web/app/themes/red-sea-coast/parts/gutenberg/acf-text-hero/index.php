<?php

/**
 * Block Hero
 *
 * @package WordPress
 * @subpackage redSeaCoast
 * @since redSeaCoast 1.0
 */
$block_object = new Block($block);
$attr = $block_object->attr();
$name = $block_object->name();

// ACF
$background = get_field('text_hero_background');
$top_text = get_field('text_hero_top_text');
$main_title = get_field('text_hero_main_title');
$subtitle = get_field('text_hero_subtitle');
$footer = get_field('text_hero_footer');

?>

<section <?php echo $attr; ?>>
  <?php load_inline_styles_plugin('swiper-bundle.min'); ?>
  <?php load_inline_styles_shared('sliders'); ?>
  <?php echo load_inline_styles(__DIR__, $name); ?>
  <?php if (!empty($background)) : ?>
    <div class="<?php echo $name . '__background' ?>">
    <?php echo wp_get_attachment_image($background, 'full'); ?>
    </div>
  <?php endif; ?>
	<div class="container">
		<div class="row justify-content-center">
      <div class="col-11 col-lg-10">
        <div class="<?php echo $name . '__content' ?>">
          <?php if (!empty($top_text)) : ?>
            <p class="<?php echo $name . '__top-text' ?>"><?php echo $top_text; ?></p>
          <?php endif; ?>
          <?php if (!empty($main_title)) : ?>
            <h1 class="<?php echo $name . '__main-title' ?>"><?php echo $main_title; ?></h1>
          <?php endif; ?>
          <?php if (!empty($subtitle)) : ?>
            <h2 class="<?php echo $name . '__subtitle' ?>"><?php echo $subtitle; ?></h2>
          <?php endif; ?>
        </div>
        <span class="<?php echo $name . '__divider' ?>">
          <?php echo get_img('small_divider', 'full') ?>
        </span>
        <?php if (!empty($footer)) : ?>
          <footer class="<?php echo $name . '__footer' ?>">
            <?php if (isset($footer['title']) && !empty($footer['title'])) : ?>
              <h3 class="<?php echo $name . '__footer-title' ?>"><?php echo $footer['title']; ?></h3>
            <?php endif; ?>
            <?php if (isset($footer['description']) && !empty($footer['description'])) : ?>
              <div class="<?php echo $name . '__footer-description' ?>">
                <?php echo $footer['description']; ?>
              </div>
            <?php endif; ?>
          </footer>
        <?php endif; ?>
      </div>
    </div>
  </div>

</section>
