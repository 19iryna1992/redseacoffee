<?php

/**
 * Block Reviews
 *
 * @package WordPress
 * @subpackage redSeaCoast
 * @since redSeaCoast 1.0
 */
$block_object = new Block($block);
$attr = $block_object->attr();
$name = $block_object->name();

// ACF
$section_title = get_field('reviews_section_title');
?>

<?php if (have_rows('reviews')) : ?>
	<section <?php echo $attr; ?>>

		<?php echo load_inline_styles(__DIR__, $name); ?>
		<?php load_inline_styles_plugin('swiper-bundle.min'); ?>
		<?php load_inline_styles_shared('sliders'); ?>

		<div class="container">
			<?php if ($section_title) : ?>
				<div class="row">
					<div class="col-12">
						<h2 class="<?php echo $name . '__title' ?>"><?php echo $section_title ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center">
				<div class="col-12 col-lg-10">
					<div class="reviews__container swiper-container <?php if (count(get_field('reviews')) > 1) : ?>reviews__swiper<?php endif; ?>" tabindex="0">
						<div class="slider__row swiper-wrapper">
							<?php while (have_rows('reviews')) : the_row();
								$rating = get_sub_field('reviews_rating');
								$content = get_sub_field('reviews_content');
								$author = get_sub_field('reviews_name');
							?>
								<div class="custom-slide swiper-slide">
									<div class="">
										<?php if ($rating) : ?>
											<div class="<?php echo $name . '__rating' ?>">
												<?php for ($i = 0; $i < $rating; $i++) : ?>
													<?php echo get_img('star', array('23', '23')) ?>
												<?php endfor; ?>
											</div>
										<?php endif; ?>
										<?php if ($content) : ?>
											<div class="<?php echo $name . '__content' ?>">
												<?php echo $content ?>
											</div>
										<?php endif; ?>
										<?php if ($author) : ?>
											<div class="<?php echo $name . '__name' ?>">
												<?php echo $author ?>
											</div>
										<?php endif; ?>
									</div>
								</div>
							<?php endwhile; ?>
						</div>

						<?php if (count(get_field('reviews')) > 1) : ?>
							<!-- Add Arrows -->
							<div class="swiper-button-next"></div>
							<div class="swiper-button-prev"></div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
