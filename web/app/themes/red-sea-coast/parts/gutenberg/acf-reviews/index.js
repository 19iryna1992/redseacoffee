function runSlider() {
	const swiper = new Swiper('.reviews__swiper', {
		loop: true,
		autoHeight: true,
		pagination: {
			el: '.swiper-pagination',
			type: 'fraction',
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
	});

  const reviewsSection = document.querySelector('.reviews');

  if (reviewsSection !== null) {
    reviewsSection.addEventListener('keydown', (e) => {
      if (e.key === "ArrowLeft") {
        swiper.slidePrev();
      } else if (e.key === "ArrowRight") {
        swiper.slideNext();
      }
    });
  }
}

document.addEventListener( 'DOMContentLoaded', () => {
	runSlider();
});

if ( window.acf ) {
	window.acf.addAction( 'render_block_preview/type=slider', runSlider);
}
