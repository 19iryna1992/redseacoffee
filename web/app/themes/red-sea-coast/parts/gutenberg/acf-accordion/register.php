<?php
$name = 'accordion';
$title = str_replace( '_', ' ', ucfirst($name) );

acf_register_block(array(
	'name'              => $name,
	'title'             => __($title, 'redSeaCoast'),
	'description'       => __("$title block", 'redSeaCoast'),
	'category'          => 'theme_blocks',
	'icon'              => 'welcome-widgets-menus',
	'mode'              => 'edit',
	'align'             => false,
	'keywords'          => array($title, 'content'),
	'supports'          => array(
		'align' => false,
		'anchor' => true,
	),
	'render_template'   => get_template_directory() . "/parts/gutenberg/acf-$name/index.php",
	'enqueue_assets'  => function() use ( $name ){
		wp_enqueue_script(
			"redSeaCoast/$name",
			get_template_directory_uri() . "/parts/gutenberg/acf-$name/index.min.js",
			[],
			filemtime( get_template_directory() . "/parts/gutenberg/acf-$name/index.min.js" ),
			true
		);
	}
));
