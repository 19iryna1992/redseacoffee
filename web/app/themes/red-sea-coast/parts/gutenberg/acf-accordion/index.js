function toggleAccordion(e) {
	e.preventDefault();
	let expanded = this.getAttribute('aria-expanded') === 'true' || false;
	this.setAttribute('aria-expanded', !expanded);
	let item = this.parentElement.nextElementSibling;
	item.hidden = !item.hidden;
}

const runAccordions = () => {
	const elements = document.querySelectorAll(".single-accordion__trigger")

	Array.from(elements).forEach( (element) => {
		element.addEventListener('click', toggleAccordion);
	});
}

runAccordions();

if ( window.acf ) {
	window.acf.addAction( 'render_block_preview/type=accordion', runAccordions);
}
