<?php
function add_table_styles( $block_content, $block ) {
	if ( 'core/table' === $block['blockName'] ) {
		$block_content = '<div data-key="core-table"></div>' . $block_content;
	}
	return $block_content;
}
add_filter( 'render_block', 'add_table_styles', 10, 2 );
