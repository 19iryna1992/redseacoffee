<?php

/**
 * Block Image Cards
 *
 * @package WordPress
 * @subpackage redSeaCoast
 * @since redSeaCoast 1.0
 */
$block_object = new Block($block);
$attr = $block_object->attr();
$name = $block_object->name();

//ACF

$video = get_field('video_file');
$poster = get_field('video_poster');

?>

<?php if ($video && $video['type'] == 'video') : ?>
	<section <?php echo $attr; ?>>
		<?php echo load_inline_styles(__DIR__, $name); ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-lg-9">
					<div class="video__wrap">
						<video width="1081" controls poster="<?php echo $poster ? $poster : '' ?>">
							<source src="<?php echo $video['url'] ?>" type="<?php echo $video['mime_type'] ?>" />
						</video>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>