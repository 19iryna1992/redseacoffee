<?php
$name = 'tiles-grid';
$title = str_replace( '_', ' ', ucfirst($name) );

acf_register_block(array(
	'name'              => $name,
	'title'             => __($title, 'redSeaCoast'),
	'description'       => __("$title block", 'redSeaCoast'),
	'category'          => 'theme_blocks',
	'icon'              => 'grid-view',
	'mode'              => 'preview',
	'align'             => false,
	'keywords'          => array($title, 'content'),
	'supports'          => array(
		'mode' => false,
		'align' => false,
		'anchor' => true,
		'align_text' => true,
		'align_content' => 'matrix',
		'jsx' => true
	),
	'render_template'   => get_template_directory() . "/parts/gutenberg/acf-$name/index.php"
));
