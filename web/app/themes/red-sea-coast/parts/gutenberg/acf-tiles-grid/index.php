<?php

/**
 * Block Tiles Grid
 *
 * @package WordPress
 * @subpackage redSeaCoast
 * @since redSeaCoast 1.0
 */
$block_object = new Block($block);
$attr = $block_object->attr();
$name = $block_object->name();

?>

<section <?php echo $attr; ?>>
  <?php load_inline_styles_plugin('swiper-bundle.min'); ?>
  <?php load_inline_styles_shared('sliders'); ?>
  <?php echo load_inline_styles(__DIR__, $name); ?>
	<div class="container">
		<div class="row justify-content-center">
      <div class="col-12 col-lg-9">
        <div class="<?php echo $name . '__items' ?>">
          <?php while(have_rows('tiles_grid_tiles')) : the_row();
            $image = get_sub_field('image');
            $image_overlay = get_sub_field('image_overlay');
            $title = get_sub_field('title');
            $link = get_sub_field('link');
          ?>
          <?php if (!empty($link)) : ?>
            <a href="<?php echo $link; ?>" class="grid-tile <?php echo $name . '__item' ?>">
          <?php else : ?>
            <article class="grid-tile <?php echo $name . '__item' ?>">
          <?php endif; ?>
              <?php if(!empty($image)) : ?>
                <div class="grid-tile__background">
                  <?php echo wp_get_attachment_image($image, 'full'); ?>
                  <?php if(!empty($image_overlay)) : ?>
                    <div class="grid-tile__background-overlay" style="background: <?php echo $image_overlay; ?>"></div>
                  <?php endif; ?>
                </div>
              <?php endif; ?>
              <?php if(!empty($title)) : ?>
                <span class="grid-tile__title"><?php echo $title; ?></span>
              <?php endif; ?>
          <?php if (!empty($link)) : ?>
            </a>
          <?php else : ?>
            </article>
          <?php endif; ?>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
  </div>

</section>
