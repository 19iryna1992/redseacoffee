<?php

/**
 * Block Image Cards
 *
 * @package WordPress
 * @subpackage redSeaCoast
 * @since redSeaCoast 1.0
 */
$block_object = new Block($block);
$attr = $block_object->attr();
$name = $block_object->name();

// ACF

$style = get_field('image_text_section_style');
$section_bg = get_field('image_text_section_background');

$i = 0;
?>

<?php if (have_rows('image_text_cards')) : ?>
	<section <?php echo $attr; ?> style="background-image: url(' <?php echo  $section_bg  ?> ') ">

	<div class="<?php echo $name . '__bg ';?>"></div>

		<?php echo load_inline_styles(__DIR__, $name); ?>

		<div class="container">
			<div class="row justify-content-center">
				<?php while (have_rows('image_text_cards')) : the_row();
					$image = get_sub_field('image');
					$title_color = get_sub_field('title_color');
					$title_white = get_sub_field('title');
					$content = get_sub_field('text');
					$link = get_sub_field('link');
					$i++;
				?>
					<div class="col-12 <?php echo ($style == 'featured') ? 'col-lg-11' : 'col-lg-9'; ?>">
						<div class="<?php echo $name . '__card ';
									echo ($style == 'featured') ? $name . '__card--featured ' : '';
									echo ($i % 2 == 0) ? $name . '__card--reverse' : '' ?>">
							<?php if ($image) : ?>
								<div class="<?php echo $name . '__img' ?>">
									<?php echo wp_get_attachment_image($image, array('550', '800')) ?>
								</div>
							<?php endif; ?>
							<div class="<?php echo $name . '__text' ?>">
								<h3 class="<?php echo $name . '__title' ?>">
									<?php if ($title_color) : ?>
										<span class="<?php echo $name . '--title-color' ?>">
											<?php echo $title_color ?>
										</span>
									<?php endif; ?>
									<?php if ($title_white) : ?>
										<?php echo $title_white ?>
									<?php endif; ?>
								</h3>
								<?php if ($content) : ?>
									<div class="<?php echo $name . '__content' ?>">
										<?php echo $content ?>
									</div>
								<?php endif; ?>
								<?php if ($link) : ?>
									<?php if ($style == 'featured') : ?>
										<?php echo get_button($link, 'link', 'image-text__link small-text__link'); ?>
									<?php else : ?>
										<?php echo get_button($link, 'outline', 'image-text__link small-text__link'); ?>
									<?php endif; ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</section>

<?php endif; ?>