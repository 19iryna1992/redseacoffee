/*global wp*/
const { __ } = wp.i18n;
const {registerBlockStyle} = wp.blocks;

const styles = [
	{
		name: 'uppercase',
		label: __('Uppercase', 'redSeaCoast')
	},
	{
		name: 'subheading',
		label: __('Subheading', 'redSeaCoast')
	},
	{
		name: 'leadparagraph',
		label: __('Leadparagraph', 'redSeaCoast')
	},
];

wp.domReady(() => {
	styles.forEach(style => {
		registerBlockStyle('core/paragraph', style);
	});
});
