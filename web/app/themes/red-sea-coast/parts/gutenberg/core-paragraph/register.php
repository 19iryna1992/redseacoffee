<?php
function insert_paragraph_styles( $block_content, $block ) {
	if ( 'core/paragraph' === $block['blockName'] ) {
		$block_content = '<div data-key="core-paragraph"></div>' . $block_content;
	}
	return $block_content;
}
add_filter( 'render_block', 'insert_paragraph_styles', 10, 2 );


function extend_block_paragraph_script() {
	wp_enqueue_script(
		'core/paragraph',
		get_template_directory_uri() . '/parts/gutenberg/core-paragraph/admin.min.js',
		array( 'wp-i18n', 'wp-dom-ready', 'wp-editor', 'wp-blocks', 'wp-compose', 'wp-element', 'wp-hooks' ),
		filemtime( get_template_directory() . '/parts/gutenberg/core-paragraph/admin.min.js' ),
	);
}
add_action( 'enqueue_block_editor_assets', 'extend_block_paragraph_script' );
