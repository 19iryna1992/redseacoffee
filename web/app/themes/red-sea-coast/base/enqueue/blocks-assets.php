<?php
/*
|--------------------------------------------------------------------------
| Load Inline Blocks Styles
|--------------------------------------------------------------------------
*/
function load_inline_styles( $path, $key, $file_name = 'style' ) {
	global $styles_arr;
	global $filter_arr;
	$file = "$path/$file_name.css";
	if ( ! in_array( $key, $styles_arr ) && file_exists( $file ) && ! in_array( $key, $filter_arr ) ) {
		$style = file_get_contents( $file );
		if ( !empty($style) ) {
			$style = check_styles_urls( $style );

			array_push( $styles_arr, $key );

			echo "<style>$style</style>";
		}
	}
}

function load_inline_dependencies($path = 'parts', $name = '', $file_name = 'style') {
	load_inline_styles( get_template_directory() .  "/$path" . '/', $name, $file_name );
}

function load_inline_styles_plugin( $key ) {
	global $styles_arr;
	$path = get_template_directory() .'/assets/css/plugins';
	$file = "$path/$key.css";

	if ( ! in_array( $key, $styles_arr ) && file_exists( $file ) ) {
		$style = file_get_contents( $file );
		if ( !empty($style) ) {
			array_push( $styles_arr, $key );

			echo "<style>$style</style>";
		}
	}
}

function load_inline_styles_shared($key) {
	global $styles_arr;
	$path = get_template_directory() ."/assets/css/shared/$key";
	$file = "$path/style.css";

	if ( !in_array( $key, $styles_arr ) && file_exists( $file ) ) {
		$style = file_get_contents( $file );

		if ( !empty($style) ) {
			$style = check_styles_urls( $style );

			array_push( $styles_arr, $key );

			echo "<style>$style</style>";
		}
	}
}

function load_styles_custom_blocks( $content ) {
	global $styles_arr;
	global $filter_arr;
	preg_match_all( '<div data-key="(.+?)".+?\/div>', $content, $matches );
	$ids = array_unique( $matches[1] );
	$path = get_template_directory() . '/parts/gutenberg';

	foreach ( $ids as $key ) {
		$key_el = '/<div data-key="' . $key . '".+?\/div>/';
		if ( ! in_array( $key, $styles_arr ) && file_exists( "$path/$key/style.css" ) ) {
			$style = file_get_contents( "$path/$key/style.css" );

			if ( !empty($style) ) {
				$html = "<style>" . check_styles_urls( $style ) . "</style>";
				$content = preg_replace ( $key_el, $html, $content, 1 );

				array_push( $filter_arr, $key );
			}
		}
		$content = preg_replace ( $key_el, '', $content );
	}

	return $content;
}
add_filter( 'the_content', 'load_styles_custom_blocks');
