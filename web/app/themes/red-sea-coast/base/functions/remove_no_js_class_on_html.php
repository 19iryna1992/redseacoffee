<?php
function change_class_on_html_when_js_is_enabled() {
	echo '<script>document.documentElement.className = document.documentElement.className.replace("no-js", "js")</script>';
}
add_action( 'wp_head', 'change_class_on_html_when_js_is_enabled' );
