<?php
/*
|--------------------------------------------------------------------------
| Store The Content
|--------------------------------------------------------------------------
*/
function store_page_content($p_obj) {
	global $styles_arr;

	$content = $p_obj->post_content;
	$value = apply_filters('the_content', $content);

	$styles_arr = [];

	return $value;
}
