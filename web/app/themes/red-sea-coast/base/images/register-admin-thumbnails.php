<?php
/*
|--------------------------------------------------------------------------
| Register All thumbnail Sizes to Admin
|--------------------------------------------------------------------------
*/
function exclude_sizes($string) {
	$disallowed_sizes = array('logo');
	foreach($disallowed_sizes as $disallowed_size) {
		if(stripos($string, $disallowed_size) !== false) {
			return false;
		}
	}
	return true;
}
function pass_sizes_admin( $sizes ) {
	$all_sizes = array();
	$custom_sizes = get_intermediate_image_sizes();
	$custom_sizes = array_filter($custom_sizes, 'exclude_sizes');
	foreach( $custom_sizes as $key => $value) {
		$all_sizes[$value] = $value;
	}
	$all_sizes = array_merge( $all_sizes, $sizes );
	return $all_sizes;
}
add_filter('image_size_names_choose', 'pass_sizes_admin', 11, 1);
