<?php
function register_theme_thumbnails() {
	global $custom_settings;
	if ( isset( $custom_settings->thumbnails ) ) {
		foreach ( $custom_settings->thumbnails as $thumb ) {
			add_image_size(
				$thumb[0],
				$thumb[1],
				$thumb[2],
				$thumb[3]
			);
		}
	}
}
add_action( 'after_setup_theme', 'register_theme_thumbnails' );
