<?php
/*
|--------------------------------------------------------------------------
| Get Template Part with vars
|--------------------------------------------------------------------------
| * Like get_template_part() put lets you pass args to the template file
| * Args are available in the tempalte as $args array
| * @param string filepart
| * @param mixed wp_args style argument list
*/

function get_part( $file, $args = array() ) {
	return get_template_part("/parts/$file", null, $args);
	// $args = wp_parse_args( $args );
	// $cache_args = wp_parse_args( $cache_args );
	// if ( $cache_args ) {
	// 	foreach ( $args as $key => $value ) {
	// 		if ( is_scalar( $value ) || is_array( $value ) ) {
	// 			$cache_args[$key] = $value;
	// 		} else if ( is_object( $value ) && method_exists( $value, 'get_id' ) ) {
	// 			$cache_args[$key] = call_user_method( 'get_id', $value );
	// 		}
	// 	}
	// 	if ( ( $cache = wp_cache_get( $file, serialize( $cache_args ) ) ) !== false ) {
	// 		if ( ! empty( $args['return'] ) )
	// 			return $cache;
	// 		echo $cache;
	// 		return;
	// 	}
	// }
	// $file_handle = $file;
	// do_action( 'start_operation', 'hm_template_part::' . $file_handle );
	// if ( file_exists( get_stylesheet_directory() . '/parts/' . $file . '.php' ) )
	// 	$file = get_stylesheet_directory() . '/parts/' . $file . '.php';
	// elseif ( file_exists( get_template_directory() . '/parts/' . $file . '.php' ) )
	// 	$file = get_template_directory() . '/parts/' . $file . '.php';
	// ob_start();
	// $return = require( $file );
	// $data = ob_get_clean();
	// do_action( 'end_operation', 'hm_template_part::' . $file_handle );
	// if ( $cache_args ) {
	// 	wp_cache_set( $file, $data, serialize( $cache_args ), 3600 );
	// }
	// if ( ! empty( $args['return'] ) )
	// 	if ( $return === false )
	// 		return false;
	// 	else
	// 		return $data;
	// return $data;
}
