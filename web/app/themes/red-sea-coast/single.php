<?php
/**
 * Single Blog Post template.
 *
 * @package    WordPress
 * @subpackage redSeaCoast
 * @since      redSeaCoast 1.0
 */
get_header();
the_post();
?>
<main id="page-content" role="main" class="page-content page-content--single">
	<div class="container">
		<div class="row">
			<div class="col-12">
			<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
	<div id="content" tabindex="-1" class="page-content__wrapper">
		<?php the_content(); ?>
	</div>
</main>
<?php
get_footer();
