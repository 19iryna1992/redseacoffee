<?php
function add_woocommerce_class_to_body($classes)
{
    if (is_page_template('archive-sale.php')) {
        $classes[] = 'woocommerce';
    }
    return $classes;
}
add_filter('body_class', 'add_woocommerce_class_to_body');
