<?php
add_action('after_setup_theme', 'woocommerce_support');

function woocommerce_support()
{
    add_theme_support('woocommerce');

}

/*---------------------------------------
 Add container to all woocommerce page
---------------------------------------*/

add_action('woocommerce_before_main_content', function () {
    echo '<div id="content" class="container">';
}, 10);

add_action('woocommerce_after_main_content', function () {
    echo '</div>';
}, 10);


function add_container_to_woocommerce_page($content)
{
    if (is_account_page()) {
        $content = '<div class="container">' . $content . '</div>';
    } 

    if (is_checkout() && !empty( is_wc_endpoint_url('order-received'))) {
        $content = '<div class="container">' . $content . '</div>';
    }

    return $content;
}

add_filter('the_content', 'add_container_to_woocommerce_page', 10);

/*---------------------------------------
 Disable woocommerce blocks
---------------------------------------*/

add_filter('allowed_block_types_all', 'disable_blocks');

function disable_blocks()
{

    $blocks = WP_Block_Type_Registry::get_instance()->get_all_registered();

    unset($blocks['woocommerce/all-reviews']);
    unset($blocks['woocommerce/featured-category']);
    unset($blocks['woocommerce/featured-product']);
    unset($blocks['woocommerce/handpicked-products']);
    unset($blocks['woocommerce/product-best-sellers']);
    unset($blocks['woocommerce/product-categories']);
    unset($blocks['woocommerce/product-category']);
    unset($blocks['woocommerce/product-new']);
    unset($blocks['woocommerce/product-on-sale']);
    unset($blocks['woocommerce/products-by-attribute']);
    unset($blocks['woocommerce/product-top-rated']);
    unset($blocks['woocommerce/reviews-by-product']);
    unset($blocks['woocommerce/reviews-by-category']);
    unset($blocks['woocommerce/product-search']);
    unset($blocks['woocommerce/product-tag']);
    unset($blocks['woocommerce/all-products']);
    unset($blocks['woocommerce/price-filter']);
    unset($blocks['woocommerce/attribute-filter']);
    unset($blocks['woocommerce/active-filters']);
    unset($blocks['woocommerce/product-search']);
    unset($blocks['woocommerce/customer-account']);
    unset($blocks['woocommerce/active-filters']);
    unset($blocks['woocommerce/filter-by-price']);
    unset($blocks['woocommerce/filter-by-stock']);
    unset($blocks['woocommerce/filter-by-attribute']);
    unset($blocks['woocommerce/filter-by-rating']);
    unset($blocks['woocommerce/mini-cart']);
    unset($blocks['woocommerce/cart']);
    unset($blocks['woocommerce/checkout']);


    return array_keys($blocks);
}


/*---------------------------------------
 Disable woocommerce blocks style
---------------------------------------*/
function deregister_woocommerce_blocks_styles() {
    wp_deregister_style( 'wc-blocks-vendors-style' );
 }
 add_action( 'wp_enqueue_scripts', 'deregister_woocommerce_blocks_styles', 100 );
 

 /*--------------------------------------
    Select Customize
 ---------------------------------------*/

 function custom_woocommerce_scripts() {
   
        wp_enqueue_style( 'select-woocommerce-styles', get_template_directory_uri() . '/assets/css/plugins/select.css' );
        wp_enqueue_script( 'select-woocommerce-script', get_template_directory_uri() . '/assets/js/plugins/select.js', array( 'jquery' ), '', true );
        wp_enqueue_script( 'customselect-woocommerce-script', get_template_directory_uri() . '/assets/js/custom-select.js', array( 'jquery' ), '', true );
        wp_enqueue_script( 'quantity-woocommerce-script', get_template_directory_uri() . '/assets/js/plugins/bootstrap-number-input.js', array( 'jquery' ), '', true );
        wp_enqueue_script( 'customquantity-woocommerce-script', get_template_directory_uri() . '/assets/js/custom-quantity.js', array( 'jquery' ), '', true );

}
add_action( 'wp_enqueue_scripts', 'custom_woocommerce_scripts' );
