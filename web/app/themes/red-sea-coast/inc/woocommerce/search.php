<?php
function add_woocommerce_class_to_search_body($classes)
{
    if (is_search()) {
        $classes[] = 'woocommerce';
    }
    return $classes;
}
add_filter('body_class', 'add_woocommerce_class_to_search_body');
