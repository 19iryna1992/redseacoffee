<?php

function remove_items_link( $items ) {
    unset( $items['dashboard'] );
    unset( $items['downloads'] );
    return $items;
}
add_filter( 'woocommerce_account_menu_items', 'remove_items_link' );
