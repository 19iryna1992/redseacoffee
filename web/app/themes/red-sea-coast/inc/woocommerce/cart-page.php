<?php

function add_container_to_cart_page( $content ) {
	if ( is_cart() ) {
		$content = '<div class="container">' . $content . '</div>';
	}
	return $content;
}
add_filter( 'the_content', 'add_container_to_cart_page', 10 );
