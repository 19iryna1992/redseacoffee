<?php

/*-----------------------------------------------
    Change Summary Item Order
-----------------------------------------------*/

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);

add_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 10);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 25);

add_filter('woocommerce_single_product_summary', 'hide_variable_price');

function hide_variable_price()
{
    global $product;

    if (!$product->is_type('variable')) {
        add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 40);
    }
}

add_filter('woocommerce_single_product_summary', 'add_score', 15);

function add_score()
{
    $score = get_field('product_single_score');

    if (!empty($score)) {
        echo '<div class="product-score">' . __('Score: ', 'redSeaCoast') . $score . '</div>';
    }
}

add_action('woocommerce_single_product_summary', 'change_single_product_ratings', 2);
function change_single_product_ratings()
{
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
    add_action('woocommerce_single_product_summary', 'single_product_ratings', 5);
}

function single_product_ratings()
{
    global $product;

    $rating_count = $product->get_rating_count();

    if ($rating_count > 0) {
        $review_count = $product->get_review_count();
        $average = $product->get_average_rating();
?>

        <div class="woocommerce-product-rating">
            <div class="woocommerce-product-rating-container">
                <span class="container-rating">
                    <?php echo wc_get_rating_html($average, $rating_count); ?>
                </span>
                <span class="woocommerce-product-rating__count">
                    <?php echo round($average, 1); ?> (<?php echo $review_count; ?>)
                </span>
            </div>
            <a href="#reviews" class="woocommerce-review-link" rel="nofollow"><?php _e('Write a Review', 'redSeaCoast') ?></a>
        </div>
<?php
    }
}

function custom_after_add_to_cart_button() {

    echo get_part('components/variation-selection-needed/index');
}
add_action( 'woocommerce_single_product_summary', 'custom_after_add_to_cart_button', 45 );


/*-----------------------------------------------
    After Single Product
-----------------------------------------------*/

remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

