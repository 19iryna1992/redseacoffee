<?php
add_filter('woocommerce_show_page_title', '__return_false');
remove_action('woocommerce_archive_description', 'woocommerce_product_archive_description', 10);
remove_action('woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10);
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);


function shop_page_navigation()
{
    if (!is_product()) {
        echo get_part('components/shop-hero/index');
    }
}

add_filter('woocommerce_before_main_content', 'shop_page_navigation', 5);


function shop_page_remove_sidebar()
{
    return false;
}

add_filter('is_active_sidebar', 'shop_page_remove_sidebar', 10, 2);



function wc_customize_product_sorting($sorting_options)
{
    $sorting_options = array(
        'menu_order' => __('Sort by', 'woocommerce'),
        'popularity' => __('Popularity', 'woocommerce'),
        'rating'     => __('Rating', 'woocommerce'),
        'price'      => __('Low to high', 'woocommerce'),
        'price-desc' => __('High to low', 'woocommerce'),
    );

    return $sorting_options;
}

add_filter('woocommerce_catalog_orderby', 'wc_customize_product_sorting');


function shop_page_content()
{
    if (is_shop()) {
        $post_id = get_option('woocommerce_shop_page_id');
        $post = get_post($post_id);
        echo apply_filters('the_content', $post->post_content);
    }
}
add_action('woocommerce_after_main_content', 'shop_page_content', 10);



function add_class_to_shop_nav_item( $classes, $item, $args ) {

	if ( 'shop_navigation' === $args->theme_location ) {
		$classes[] = "swiper-slide";
    }
	return $classes;
}

add_filter( 'nav_menu_css_class' , 'add_class_to_shop_nav_item' , 10, 4 );
