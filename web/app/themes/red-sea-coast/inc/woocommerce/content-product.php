<?php

function add_share_batton()
{
    echo get_part('components/shared-button/index');
}
add_action('woocommerce_before_shop_loop_item', 'add_share_batton', 5);


function wrap_thumbnail_with_div()
{
    echo '<div class="attachment-woocommerce_thumbnail-wrapper">';
}
add_action('woocommerce_before_shop_loop_item_title', 'wrap_thumbnail_with_div', 5);


function wrap_thumbnail_with_div_close()
{
    echo '<div class="button">Learn More</div>';
    echo '</div>';
}
add_action('woocommerce_before_shop_loop_item_title', 'wrap_thumbnail_with_div_close', 15);


function add_rating_before_title()
{
    global $product;

    echo $product->get_average_rating() > 0 ? '<div class="woocommerce-product-rating">' : '<div class="woocommerce-product-rating" style="height: 15px">';
    echo wc_get_rating_html($product->get_average_rating());
    echo '</div>';
}
add_action('woocommerce_shop_loop_item_title', 'add_rating_before_title', 5);

remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);


function hide_sale_flash()
{
    return false;
}

add_filter('woocommerce_sale_flash', 'hide_sale_flash');



