<?php

/**
 * Search Page template
 *
 * @package WordPress
 * @subpackage redSeaCoast
 * @since redSeaCoast 1.0
 */
get_header();
the_post();
?>

<main id="page-content" role="main" class="page-content page-content--search">
    <div id="content" tabindex="-1" class="page-content__wrapper">
        <div class="container">
            <?php echo get_part('search/search-result/index'); ?>
        </div>
    </div>
</main>
<?php
get_footer();
