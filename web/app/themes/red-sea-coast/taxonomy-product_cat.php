<?php
/*
    Template Name: Custom Product Category
    Description: This is a custom template for the WooCommerce product category archive page.
    */

get_header();
$current_cat = get_queried_object();
$page_content_id = get_field('taxonomy_page_id', $current_cat);
$enable_product_list = get_field('taxonomy_enable_product_list', $current_cat);
$enable_shop_hero = get_field('taxonomy_enable_shop_hero', $current_cat);

if ($enable_shop_hero) {
    echo get_part('components/shop-hero/index');
}

?>

<main id="page-content" role="main" class="page-content page-content page-content--default page-content--product-cat">
    <h1 class="page-title screen-reader-text"><?php the_title(); ?></h1>
    <div id="content" tabindex="-1" class="page-content__wrapper">
        <div class="container">

            <?php if ($enable_product_list) {
                woocommerce_product_loop_start();

                $sale_args = array(
                    'post_type'      => 'product',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'product_cat',
                            'field' => 'term_id',
                            'terms' => $current_cat->term_id,
                        )
                    ),
                    'posts_per_page' => 12,
                    'orderby'        => 'meta_value_num',
                    'order'          => 'ASC'
                );

                $sale_products = new WP_Query($sale_args);

                while ($sale_products->have_posts()) : $sale_products->the_post();
                    wc_get_template_part('content', 'product');
                endwhile;
                woocommerce_product_loop_end();
            }

            if (is_object($page_content_id)) {
                echo apply_filters('the_content', $page_content_id->post_content);
            }

            wp_reset_postdata(); ?>
        </div>
    </div>
</main>
<?php
get_footer();
