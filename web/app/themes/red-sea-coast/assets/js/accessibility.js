// Single Product Swatcher

const swatchlyWrap = document.querySelectorAll('.swatchly-swatch');

const swatchlySwatch = document.querySelectorAll('.swatchly-swatch');

if (swatchlySwatch) {
    swatchlySwatch.forEach(swatch => {
        swatch.setAttribute('role', 'button');
        swatch.setAttribute('tabindex', '0');

        swatch.addEventListener('keydown', e => {
            if (e.key === "Tab") {
                swatch.focus();
            }
        });
    });
}

// Skip Content

const skipLink = document.querySelector('.skip-link');
const content = document.getElementById('content');


skipLink.addEventListener('keydown', (e) => {
    if (e.key === "Enter") {
        scrollToResolver(content);
    }
});

function scrollToResolver(elem) {
    const jump = parseInt(elem.getBoundingClientRect().top - 1500);
    document.body.scrollTop += jump;
    document.documentElement.scrollTop += jump;
    if (!elem.lastjump || elem.lastjump > Math.abs(jump)) {
        elem.lastjump = Math.abs(jump);
        setTimeout(function () {
            scrollToResolver(elem);
            elem.focus();
        }, "100");
    } else {
        elem.lastjump = null;
    }
}
