(function ($) {
    $(document).ready(function () {
        $('input[type="number"]').bootstrapNumber();

        $('input[type="number"]').bootstrapNumber({
            // default, danger, success , warning, info, primary
            upClass: 'danger',
            downClass: 'success',
            center: true
        });


        $('.JS-event').click(function () {
            $('.JS-modal').toggleClass('d-none');
        });
        $('.JS-modal').click(function () {
            $(this).toggleClass('d-none');
        });
    });
})(jQuery);