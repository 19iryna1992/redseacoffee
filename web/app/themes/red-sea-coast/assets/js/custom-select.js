(function ($) {
	$(document).ready(function () {

		if ($('select') && $('.woocommerce')) {
			$('select').select2();
			$('select').one('select2:open', function (e) {
				$('input.select2-search__field').prop('placeholder', 'Search');
			});
		}
	});
})(jQuery);