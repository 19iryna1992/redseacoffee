<?php

/**
 * Template Name: Sale Products Archive
 *
 * @package    WordPress
 * @subpackage redSeaCoast
 * @since      redSeaCoast 1.0
 */

get_header();
//the_post();

$current_page = get_the_ID();
$page_content = get_post($current_page);

$enable_product_list = get_field('enable_sales_product', 'options');
$enable_shop_hero = get_field('enable_shop_hero', 'options');

if($enable_shop_hero){
    echo get_part('components/shop-hero/index');
}

?>
<main id="page-content" role="main" class="page-content page-content--default">
    <div id="content" tabindex="-1" class="page-content__wrapper">
        <div class="container">

            <?php if ($enable_product_list) {
                woocommerce_product_loop_start();

                $sale_args = array(
                    'post_type'      => 'product',
                    'meta_query'     => array(
                        array(
                            'key'       => '_sale_price',
                            'value'     => 0,
                            'compare'   => '>',
                            'type'      => 'NUMERIC'
                        )
                    ),
                    'posts_per_page' => 12,
                    'orderby'        => 'meta_value_num',
                    'meta_key'       => '_sale_price',
                    'order'          => 'ASC'
                );

                $sale_products = new WP_Query($sale_args);

                while ($sale_products->have_posts()) : $sale_products->the_post();
                    wc_get_template_part('content', 'product');
                endwhile;
                woocommerce_product_loop_end();
            }

            if (is_object($page_content)) {
                echo apply_filters('the_content', $page_content->post_content);
            };

            wp_reset_postdata();
            ?>
        </div>
    </div>

</main>
<?php
get_footer();
